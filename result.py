import os
import cv2
import face_recognition
from sklearn import svm
from sklearn.metrics import accuracy_score, f1_score
import matplotlib.pyplot as plt
import pickle

def train_svm():
    # Training the SVC classifier
    encodings = []
    names = []
    train_dir = os.listdir('F:/Face-Recognition-Using-SVM-master/train/')
    a=[]
    # Train SVM using faces given in train directory
    for person in train_dir:
        person_folder = os.path.join('F:/Face-Recognition-Using-SVM-master/train/', person)
        pix = os.listdir(person_folder)
        for person_img in pix:
            img_path = os.path.join(person_folder, person_img)
            face = face_recognition.load_image_file(img_path)
            face_bounding_boxes = face_recognition.face_locations(face)

            if len(face_bounding_boxes) == 1:
                face_enc = face_recognition.face_encodings(face)[0]
                encodings.append(face_enc)
                names.append(person)
            else:
                print(person_img +" in " + person_folder + " was skipped and can't be used for training")
                a.append(str(person_img))
        print("Training using "+ person + "Finished")
    clf = svm.SVC(gamma='scale')
    clf.fit(encodings, names)
    # saving pickle file
    with open('svm_model.pkl', 'wb') as f:
        pickle.dump(clf, f)
    print(a)
    # with open('svm_model.pkl', 'rb') as f:
    #     clf = pickle.load(f)
    # names=[]
    print("Train finished")
    return clf, names

def test_svm(clf, test_folder):
    # Testing SVM classifier
    accuracy_list = []
    f1_list = []
    folders = sorted(os.listdir(test_folder))
    k=0
    for folder in folders:
        path = os.path.join(test_folder, folder)
        true_names = []
        predicted_names = []

        for test_image_file in os.listdir(path):
            test_image_path = os.path.join(path, test_image_file)
            test_image = face_recognition.load_image_file(test_image_path)
            face_locations = face_recognition.face_locations(test_image)
            num = len(face_locations)

            for i in range(num):
                test_image_enc = face_recognition.face_encodings(test_image)[i]
                name = clf.predict([test_image_enc])
                predicted_names.append(*name)
                true_names.append(test_image_file.split('.')[0])
        # Finding accuracy and F1score        
        accuracy = accuracy_score(true_names, predicted_names)*100
        f1score = f1_score(true_names, predicted_names, average='weighted',zero_division=1)*100

        accuracy_list.append(accuracy)
        f1_list.append(f1score)
        
        k=k+1
        print("Test No "+str(k)+" finished ::  Accuracy: "+str(accuracy)+"   F1score: "+str(f1score))

    return accuracy_list, f1_list

def plot_results(accuracy_list, f1_list):
    # Plot accuracy and f1score from each testset
    tests = list(range(1, len(accuracy_list) + 1))

    plt.plot(tests, accuracy_list, label='Accuracy')
    plt.plot(tests, f1_list, label='F1score')

    plt.xlabel('Test')
    plt.ylabel('Score')
    plt.title('Performance Metrics Across Test Dataset')
    plt.legend()
    plt.show()

def train_and_test():
    clf, _ = train_svm()
    accuracy_list, f1_list= test_svm(clf, 'F:/Face-Recognition-Using-SVM-master/test/')
    plot_results(accuracy_list, f1_list)
    print("Max Accuracy :" + str(max(accuracy_list)))
    print("Max F1score :" + str(max(f1_list)))
    print("Avg Accuracy :" + str(sum(accuracy_list)/len(accuracy_list)))
    print("Avg F1score :" + str(sum(f1_list)/len(f1_list)))

if __name__ == "__main__":
    train_and_test()
